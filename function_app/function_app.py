import azure.functions as func
from math import sin
import logging

def integrate(func, low, up, n):
    integral = 0
    dx = (up-low)/n
    for i in range(n):
        integral += func(dx*(i+0.5))*dx
        i += dx
    return integral

app = func.FunctionApp(http_auth_level=func.AuthLevel.ANONYMOUS)

@app.route(route="IntegrateApp")
def IntegrateApp(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    up = float(req.params.get('up'))
    if not up:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            up = float(req_body.get('up'))
    
    low = float(req.params.get('low'))
    if not low:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            low = float(req_body.get('low'))

    f = sin
    result = ""
    for i in range(1,8):
        result += f"<br>N = 10^{i}<br>I = {integrate(f, low, up, 10**i)}<br>"

    return func.HttpResponse(result)
