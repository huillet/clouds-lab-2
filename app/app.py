from flask import Flask
from integration import integrate
from math import sin

app = Flask(__name__)

@app.route("/integrate/<low>/<up>")
def integrate_page(low, up):
    low = float(low)
    up = float(up)
    func = sin
    result = ""
    for i in range(1,8):
        result += f"<br>N = 10^{i}<br>I = {integrate(func, low, up, 10**i)}<br>"
    return result

app.run("0.0.0.0", 8080, True)
