from math import sin, pi

def integrate(func, low, up, n):
    integral = 0
    dx = (up-low)/n
    for i in range(n):
        integral += func(dx*(i+0.5))*dx
        i += dx
    return integral

def main():
    func = sin
    low = 0
    up = pi
    for i in range(1,8):
        print(f"N = 10^{i}    I = {integrate(func, low, up, 10**i)}")

if __name__ == "__main__":
    main()
